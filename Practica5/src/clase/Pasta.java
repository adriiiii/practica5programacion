package clase;

/**
*
* @author Adrian Rodriguez Escalona
*
*/
public class Pasta extends Producto{
	private String salsa;
	private String tipoPasta;
	private double tiempoCoccion;
	
	/**
	 *Es el constructor de los atributos del padre Productos y del hijo Pasta
	 * @param precio
	 * @param sabor
	 * @param nombre
	 * @param tamano
	 * @param salsa
	 * @param tipoPasta
	 * @param tiempoCoccion
	 */
	public Pasta(double precio, String sabor, String nombre, double tamano, String salsa, String tipoPasta,double tiempoCoccion) {
		super(precio, sabor, nombre, tamano);
		this.salsa = salsa;
		this.tipoPasta = tipoPasta;
		this.tiempoCoccion = tiempoCoccion;
		
	}

	/**
	 * El get de salsa de la pasta
	 * @return
	 */
	public String getSalsa() {
		return salsa;
	}

	/**
	 * El set de salsa de la pasta
	 * @param salsa
	 */
	public void setSalsa(String salsa) {
		this.salsa = salsa;
	}

	/**
	 * El get del tipo de pasta 
	 * @return
	 */
	public String getTipoPasta() {
		return tipoPasta;
	}

	/**
	 * El set del tipo de pasta
	 * @param tipoPasta
	 */
	public void setTipoPasta(String tipoPasta) {
		this.tipoPasta = tipoPasta;
	}

	/**
	 * El get del tiempo de coccion de la pasta
	 * @return
	 */
	public double getTiempoCoccion() {
		return tiempoCoccion;
	}
	/**
	 * El set del tiempo de coccion de la pasta
	 * @param tiempoCoccion
	 */

	public void setTiempoCoccion(double tiempoCoccion) {
		this.tiempoCoccion = tiempoCoccion;
	}


	
	/**
	 * El toString de la calse pasta
	 */
	
	@Override
	public String toString() {
		return "Pasta [salsa=" + salsa + ", tipoPasta=" + tipoPasta + ", tiempoCoccion=" + tiempoCoccion + ", nombre="
				+ nombre + ", precio=" + precio + ", sabor=" + sabor + ", tamano=" + tamano + "]";
	}

	
	
	
	//Si es menor de 7 min esta poco hecho
}
