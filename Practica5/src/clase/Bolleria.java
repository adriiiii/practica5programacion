package clase;
/**
*
* @author Adrian Rodriguez Escalona
*
*/
public class Bolleria extends Producto {
	private String tipoBolleria;
	private boolean esDulce;
	private boolean conVirutas;
/**
 * Es el constructor de los atributos del padre Productos y del hijo Bolleria
 * @param precio
 * @param sabor
 * @param nombre
 * @param tamano
 * @param tipoBolleria
 * @param esDulce
 * @param conVirutas
 */
	public Bolleria(double precio, String sabor, String nombre, double tamano, String tipoBolleria, boolean esDulce, boolean conVirutas) {
		super(precio, sabor, nombre, tamano);
		this.tipoBolleria = tipoBolleria;
		this.esDulce = esDulce;
		this.conVirutas = conVirutas;
	}
		
	/**
	* El get del tipo de bolleria
	* @return
	*/
	public String getTipoBolleria() {
		return tipoBolleria;
	}
	/**
	 * El set de tipo de bolleria
	 * @param tipoBolleria
	 */
	public void setTipoBolleria(String tipoBolleria) {
		this.tipoBolleria = tipoBolleria;
	}
	
	/**
	 * El get de es dulce pero al ser un booleano pone isEsDulce
	 * @return
	 */
	public boolean isEsDulce() {
		return esDulce;
	}
	/**
	 * El set de el dulce
	 * @param esDulce
	 */
	public void setEsDulce(boolean esDulce) {
		this.esDulce = esDulce;
	}
	/**
	 * El get de con virutas pero al ser un booleano pone isConVirutas
	 * @return
	 */
	public boolean isConVirutas() {
		return conVirutas;
	}
	/**
	 * El set de con virutas
	 * @param conVirutas
	 */
	public void setConVirutas(boolean conVirutas) {
		this.conVirutas = conVirutas;
	}
	
	
	/**
	 * El toString de la clase bolleria
	 */
	@Override
	public String toString() {
		return "Bolleria [tipoBolleria=" + tipoBolleria + ", esDulce=" + esDulce + ", conVirutas=" + conVirutas
				+ ", nombre=" + nombre + ", precio=" + precio + ", sabor=" + sabor + ", tamano=" + tamano + "]";
	}
	
}