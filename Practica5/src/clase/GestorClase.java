package clase;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Adrian Rodriguez Escalona
 *
 */
public class GestorClase {
	private ArrayList<Pastel> listaPasteles;
	private ArrayList<Bocadillo> listaBocadillos;
	private ArrayList<Pasta> listaPasta;
	private ArrayList<Bolleria> listaBolleria;
	private ArrayList<Producto> listaProductos;

	/**
	 * Los arrayList de las clases
	 */
	public GestorClase() {
		this.listaPasteles = new ArrayList<Pastel>();
		this.listaBocadillos = new ArrayList<Bocadillo>();
		this.listaPasta = new ArrayList<Pasta>();
		this.listaBolleria = new ArrayList<Bolleria>();
		this.listaProductos = new ArrayList<Producto>();
	}
	
	/**
	 * El alta de pasteles
	 * @param precio
	 * @param sabor
	 * @param nombre
	 * @param tamano
	 * @param tipoPastel
	 * @param peso
	 * @param figura
	 */
	public void altaPasteles(double precio, String sabor, String nombre, double tamano, String tipoPastel, double peso,
			String figura) {
		if (!existePastel(nombre)) {
			Pastel nuevoPastel = new Pastel(precio, sabor, nombre, tamano, tipoPastel, peso, figura);
			listaPasteles.add(nuevoPastel);
			System.out.println("Nuevo pastel registrado");
		} else {
			System.out.println("El pastel ya esta registrado");
		}
	}
	/**
	 * Un metodo que comprueba si existe un pastel
	 * @param nombre
	 * @return
	 */
	public boolean existePastel(String nombre) {
		for (Pastel pastel : listaPasteles) {
			if (pastel != null && pastel.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Metodo de listar pasteles
	 */
	public void listarPasteles() {
		for (Pastel pastel : listaPasteles) {
			if (pastel != null) {
				System.out.println(pastel);
			}
		}
	}
	/**
	 * Metodo buscar pasteles
	 * @param nombre
	 * @return
	 */
	public Pastel buscarPasteles(String nombre) {
		for (Pastel pastel : listaPasteles) {
			if (pastel != null && pastel.getNombre().equals(nombre)) {
				return pastel;
			}
		}
		return null;
	}
	/**
	 * Metodo eliminar pastel por su nombre
	 * @param nombre
	 */
	public void eliminarPastel(String nombre) {
		Iterator<Pastel> iteradorPastel = listaPasteles.iterator();
		while (iteradorPastel.hasNext()) {
			Pastel pastel = iteradorPastel.next();
			if (pastel.getNombre().equals(nombre)) {
				iteradorPastel.remove();
			}
		}
	}
	/**
	 * Metodo alta de bocadillos
	 * @param precio
	 * @param sabor
	 * @param nombre
	 * @param tamano
	 * @param tipoPan
	 * @param esSalado
	 * @param numCortes
	 * @param paraCeliaco
	 */
	public void altaBocadillos(double precio, String sabor, String nombre, double tamano, String tipoPan,
			boolean esSalado, int numCortes, boolean paraCeliaco) {
		if (!existeBocadillo(nombre)) {
			Bocadillo nuevoBocadillo = new Bocadillo(precio, sabor, nombre, tamano, tipoPan, esSalado, numCortes,
					paraCeliaco);
			listaBocadillos.add(nuevoBocadillo);
			System.out.println("Nuevo bocadillo registrado");
		} else {
			System.out.println("El bocadillo ya esta registrado");
		}
	}
	/**
	 * Metodo que comprueba si existe el bocadillo
	 * @param nombre
	 * @return
	 */
	public boolean existeBocadillo(String nombre) {
		for (Bocadillo bocadillo : listaBocadillos) {
			if (bocadillo != null && bocadillo.getNombre().equalsIgnoreCase(nombre)) {
				return true;
			}
		}
		return false;
	}

	
	/**
	 * Metodo listar bocadillo
	 */
	public void listarBocadillos() {
		for (Bocadillo bocadillo : listaBocadillos) {
			if (bocadillo != null) {
				System.out.println(bocadillo);
			}
		}
	}
	/**
	 * Metodo buscar bocadillo
	 * @param nombre
	 * @return
	 */
	public Bocadillo buscarBocadillo(String nombre) {
		for (Bocadillo bocadillo : listaBocadillos) {
			if (bocadillo != null && bocadillo.getNombre().equals(nombre)) {
				return bocadillo;
			}
		}
		return null;
	}
	/**
	 * Metodo eliminar un bocadillo por su nombre
	 * @param nombre
	 */
	public void eliminarBocadillo(String nombre) {
		Iterator<Bocadillo> iteradorBocadillo = listaBocadillos.iterator();
		while (iteradorBocadillo.hasNext()) {
			Bocadillo bocadillo = iteradorBocadillo.next();
			if (bocadillo.getNombre().equals(nombre)) {
				iteradorBocadillo.remove();
			}
		}
	}
	/**
	 * Metodo alta de pastas
	 * @param precio
	 * @param sabor
	 * @param nombre
	 * @param tamano
	 * @param salsa
	 * @param tipoPasta
	 * @param tiempoCoccion
	 */
	public void altaPastas(double precio, String sabor, String nombre, double tamano, String salsa, String tipoPasta,
			double tiempoCoccion) {
		if (!existePasta(nombre)) {
			Pasta nuevoPasta = new Pasta(precio, sabor, nombre, tamano, salsa, tipoPasta, tiempoCoccion);
			listaPasta.add(nuevoPasta);
			System.out.println("Nuevo Pasta registrado");
		} else {
			System.out.println("El Pasta ya esta registrado");
		}
	}
	/**
	 * Metodo que comprueba si pasta existe
	 * @param nombre
	 * @return
	 */
	public boolean existePasta(String nombre) {
		for (Pasta Pasta : listaPasta) {
			if (Pasta != null && Pasta.getNombre().equalsIgnoreCase(nombre)) {
				return true;
			}
		}
		return false;
	}

	
	/**
	 * Metodo listar pastas
	 */
	public void listarPastas() {
		for (Pasta Pasta : listaPasta) {
			if (Pasta != null) {
				System.out.println(Pasta);
			}
		}
	}
	/**
	 * Metodo buscar pasta por su nombre
	 * @param nombre
	 * @return
	 */
	public Pasta buscarPasta(String nombre) {
		for (Pasta Pasta : listaPasta) {
			if (Pasta != null && Pasta.getNombre().equals(nombre)) {
				return Pasta;
			}
		}
		return null;
	}
	/**
	 * Metodo eliminar pasta por su nombre
	 * @param nombre
	 */
	public void eliminarPasta(String nombre) {
		Iterator<Pasta> iteradorPasta = listaPasta.iterator();
		while (iteradorPasta.hasNext()) {
			Pasta Pasta = iteradorPasta.next();
			if (Pasta.getNombre().equals(nombre)) {
				iteradorPasta.remove();
			}
		}
	}
	/**
	 * Metodo de alta de bollerias
	 * @param precio
	 * @param sabor
	 * @param nombre
	 * @param tamano
	 * @param tipoBolleria
	 * @param esDulce
	 * @param conVirutas
	 */
	public void altaBollerias(double precio, String sabor, String nombre, double tamano, String tipoBolleria,
			boolean esDulce, boolean conVirutas) {
		if (!existeBolleria(nombre)) {
			Bolleria nuevoBolleria = new Bolleria(precio, sabor, nombre, tamano, tipoBolleria, esDulce, conVirutas);
			listaBolleria.add(nuevoBolleria);
			System.out.println("Nuevo Bolleria registrado");
		} else {
			System.out.println("El Bolleria ya esta registrado");
		}
	}
	/**
	 * Metodo de que si existe un bollo
	 * @param nombre
	 * @return
	 */
	public boolean existeBolleria(String nombre) {
		for (Bolleria Bolleria : listaBolleria) {
			if (Bolleria != null && Bolleria.getNombre().equalsIgnoreCase(nombre)) {
				return true;
			}
		}
		return false;
	}

	
	/**
	 * Metodo listar bolleria
	 */
	public void listarBollerias() {
		for (Bolleria Bolleria : listaBolleria) {
			if (Bolleria != null) {
				System.out.println(Bolleria);
			}
		}
	}
	/**
	 * Metodo buscar bolleria por su nombre
	 * @param nombre
	 * @return
	 */
	public Bolleria buscarBolleria(String nombre) {
		for (Bolleria Bolleria : listaBolleria) {
			if (Bolleria != null && Bolleria.getNombre().equals(nombre)) {
				return Bolleria;
			}
		}
		return null;
	}
	/**
	 * Metodo eliminar bollo por su nombre
	 * @param nombre
	 */
	public void eliminarBolleria(String nombre) {
		Iterator<Bolleria> iteradorBolleria = listaBolleria.iterator();
		while (iteradorBolleria.hasNext()) {
			Bolleria Bolleria = iteradorBolleria.next();
			if (Bolleria.getNombre().equals(nombre)) {
				iteradorBolleria.remove();
			}
		}
	}
	

}
