package clase;
/**
*
* @author Adrian Rodriguez Escalona
*
*/
public class Bar {
	private String nombreBar;
	private double metros;
	private int numPedido;
	
	/**
	 * Constructor de los atributos de bar
	 * @param nombreBar
	 * @param metros
	 * @param numPedido
	 */
	public Bar(String nombreBar, double metros, int numPedido) {
		this.nombreBar = nombreBar;
		this.metros = metros;
		this.numPedido = numPedido;
	}
	/**
	 * El get del nombre del bar
	 * @return
	 */

	public String getNombreBar() {
		return nombreBar;
	}
	/**
	 * El set del nombre del bar
	 * @param nombreBar
	 */

	public void setNombreBar(String nombreBar) {
		this.nombreBar = nombreBar;
	}
	/**
	 * El get de los metros que tiene el bar
	 * @return
	 */

	public double getMetros() {
		return metros;
	}
	/**
	 * El set de metro del bar
	 * @param metros
	 */

	public void setMetros(double metros) {
		this.metros = metros;
	}

	/**
	 * El get del numero del pedido
	 * @return
	 */
	public int getNumPedido() {
		return numPedido;
	}

	/**
	 * El set del numero del pedido
	 * @param numPedido
	 */
	public void setNumPedido(int numPedido) {
		this.numPedido = numPedido;
	}

	/**
	 * Super de la case1 Bar
	 */
	@Override
	public String toString() {
		return "Bar [nombreBar=" + nombreBar + ", metros=" + metros + ", numPedido=" + numPedido + "]";
	}
	
	
	
	
}
