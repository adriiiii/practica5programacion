package clase;

import java.time.LocalDate;

/**
 *
 * @author Adrian Rodriguez Escalona
 *
 */
public class Bocadillo extends Producto{
	private String tipoPan;
	private boolean esSalado;
	private int numCortes;
	private boolean paraCeliaco;

	/**
	 * Es el constructor de los atributos del padre Productos y del hijo Bocadillo
	 * @param precio
	 * @param sabor
	 * @param nombre
	 * @param tamano
	 * @param tipoPan
	 * @param esSalado
	 * @param numCortes
	 * @param paraCeliaco
	 */

	public Bocadillo(double precio, String sabor, String nombre, double tamano, String tipoPan, boolean esSalado, int numCortes, boolean paraCeliaco) {
		super(precio, sabor, nombre, tamano);
		this.tipoPan = tipoPan;
		this.esSalado = esSalado;
		this.numCortes = numCortes;
		this.paraCeliaco = paraCeliaco;
	}

	/**
	 * El get del tipo de pan del bocadillo
	 * @return
	 */
	public String getTipoPan() {
		return tipoPan;
	}

	/**
	 * El set del tipo de pan del bocadillo
	 * @param tipoPan
	 */
	public void setTipoPan(String tipoPan) {
		this.tipoPan = tipoPan;
	}

	/**
	 * El get de es dulce pero al ser un booleano pone isEsDulce
	 * @return
	 */
	public boolean isEsDulce() {
		return esSalado;
	}

	/**
	 * El set de es dulce del bocadillo
	 * @param esDulce
	 */
	public void setEsDulce(boolean esDulce) {
		this.esSalado = esDulce;
	}

	/**
	 * El get de numero de cortes del bocadillo
	 * @return
	 */
	public int getNumCortes() {
		return numCortes;
	}

	/**
	 * El set del numero de cortes del bocadillo
	 * @param numCortes
	 */
	public void setNumCortes(int numCortes) {
		this.numCortes = numCortes;
	}

	/**
	 * El get de que si es para celiacos pero al ser un booleano pone isParaCeliaco
	 * @return
	 */
	public boolean isParaCeliaco() {
		return paraCeliaco;
	}

	/**
	 * El set de que si es para celiaco 
	 * @param paraCeliaco
	 */
	public void setParaCeliaco(boolean paraCeliaco) {
		this.paraCeliaco = paraCeliaco;
	}

	/**
	 * El toString de la calse bocadillo
	 */

	@Override
	public String toString() {
		return "Bocadillo [tipoPan=" + tipoPan + ", esSalado=" + esSalado + ", numCortes=" + numCortes
				+ ", paraCeliaco=" + paraCeliaco + ", nombre=" + nombre + ", precio=" + precio + ", sabor=" + sabor
				+ ", tamano=" + tamano + "]";
	}
	
	/**
	 * Metodo que si el pan es tipo nieve, se sumara un euro mas
	 */
	
	//Si el pan es tipo nieve +1 euro
	public void tipoPanNieve() {
		if (getTipoPan().equalsIgnoreCase("Pan de nieve")) {
			setPrecio(getPrecio()+1);
		} else if (getTipoPan().equalsIgnoreCase("Masa madre")) {
			setPrecio(getPrecio()+2);
		}
	}
	

}