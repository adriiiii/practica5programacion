package clase;

import java.time.LocalDate;

/**
 *
 * @author Adrian Rodriguez Escalona
 *
 */
public class Pastel extends Producto{
	private String tipoPastel;
	private double peso;
	private String figura;

	/**
	 * Es el constructor de los atributos del padre Productos y del hijo Pastel
	 * @param precio
	 * @param sabor
	 * @param nombre
	 * @param tamano
	 * @param tipoPastel
	 * @param peso
	 * @param figura
	 */
	
	public Pastel(double precio, String sabor, String nombre, double tamano, String tipoPastel, double peso, String figura) {
		super(precio, sabor, nombre, tamano);
		this.tipoPastel = tipoPastel;
		this.peso = peso;
		this.figura = figura;
	}


/**
 * Get del tipo de pastel
 * @return
 */
	public String getTipoPastel() {
		return tipoPastel;
	}


/**
 * Set del tipo de pastel
 * @param tipoPastel
 */
	public void setTipoPastel(String tipoPastel) {
		this.tipoPastel = tipoPastel;
	}
/**
 * Get del peso de pastel
 * @return
 */

	public double getPeso() {
		return peso;
	}
/**
 * Set del peso del pastel
 * @param peso
 */

	public void setPeso(double peso) {
		this.peso = peso;
	}
/**
 * El get de la figura del pastel
 * @return
 */

	public String getFigura() {
		return figura;
	}

	/**
	 * El set de la figura del pastel
	 * @param figura
	 */
	public void setFigura(String figura) {
		this.figura = figura;
	}
	


	/**
	 * El toString del metodo pastel
	 */
@Override
	public String toString() {
		return "Pastel [tipoPastel=" + tipoPastel + ", peso=" + peso + ", figura=" + figura + ", nombre=" + nombre
				+ ", precio=" + precio + ", sabor=" + sabor + ", tamano=" + tamano +"]";
	}

	/**
	 * Un metodo que si el peso es mayor que el tamano, se multiplica *2
	 * @param peso
	 * @param tamano
	 */
	
	//si el peso es mayor del tamano *2
	public void pesoMayor() {
		if (getPeso()>=getTamano()) {
			setTamano(getTamano()*2);
		}
	}
	
	

	
}