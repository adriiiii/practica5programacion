package clase;

import java.time.LocalDate;
/**
*
* @author Adrian Rodriguez Escalona
*
*/
public class Producto {
	String nombre;
	double precio;
	String sabor;
	double tamano;

	
/**
 * Es el constructor de los atributos del Padre de las herencias
 * @param precio
 * @param sabor
 * @param nombre
 * @param tamano
 */
	public Producto(double precio, String sabor, String nombre, double tamano) {
		this.nombre= nombre;
		this.precio = precio;
		this.sabor = sabor;
		this.tamano=tamano;
	}

	/**
	 * El get del nombre
	 * @return
	 */

	public String getNombre() {
		return nombre;
	}
	/**
	 * El set del nombre
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * El get del precio
	 * @return
	 */
	public double getPrecio() {
		return precio;
	}
	/**
	 * El set del precio
	 * @param precio
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	/**
	 * El get de sabor
	 * @return
	 */
	public String getSabor() {
		return sabor;
	}
	/**
	 * El set de sabor
	 * @param sabor
	 */
	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	
	/**
	 * El get del tamano 
	 * @return
	 */

	public double getTamano() {
		return tamano;
	}
	/**
	 * El set del tamano
	 * @param tamano
	 */
	public void setTamano(double tamano) {
		this.tamano = tamano;
	}
	
	
	/**
	 * El toString del producto
	 */
	@Override
	public String toString() {
		return "Producto [nombre=" + nombre + ", precio=" + precio + ", sabor=" + sabor + ", tamano=" + tamano
				+ "]";
	}


	
	
	

}
