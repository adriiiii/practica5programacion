package programa;

import java.util.Scanner;
import clase.GestorClase;

/**
 *
 * @author Adrian Rodriguez Escalona
 *
 */
public class Programa {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int opcion = 0;

		GestorClase gestor = new GestorClase();
		
		
		gestor.altaPasteles(15.30, "Fresa", "Locura de fresa", 10.00, "Cremoso", 16.60, "Corazon");
		gestor.altaPasteles(13.50,"Platano","Ida de olla de platano", 12.00, "Fermentado", 14.60, "Mono");
		gestor.altaPasteles(15.50,"Macedonia","Demencia de macedonia", 10.00,"Esponjoso", 16.30,"Flor");
		gestor.altaPasteles(13.50,"Limon","Calaverada mortal de limon", 12.00,"Fermentado", 14.60, "Abeja");
		gestor.altaPasteles(17.50,"Manzana","Frenesi de manzana", 15.00,"Natilla", 17.00, "Nino");
		gestor.altaPasteles(17.80,"Vainilla","Explosion de Vainilla", 15.99,"Cremoso",17.30,"Helado");
		gestor.altaPasteles(18.50,"Bromita","Extrema payaseria de MarianoRajoy", 16.00, "Esponjoso",18.00,"Libro");

		
		gestor.altaBocadillos(7.50,"Jamon, tomate y aceite","Demencia furiosa de jamon", 9.00, "Pan de nieve", true, 4,true);
		gestor.altaBocadillos(10.50, "Lomo y queso", "Vesania de lomo y queso", 9.00, "Masa madre", true, 5, false);
		gestor.altaBocadillos(11.00, "Bacon, queso y huevo", "Extravagancia elegante de bacon, queso y huevo", 10.00, "Pan gallego", true, 6, true);
		gestor.altaBocadillos(6.50, "Fuet", "Vesania de lomo y queso", 9.00, "Pan de nueve", true, 3, false);
		gestor.altaBocadillos(10.00, "Vegetal", "Insensatez infrahumana de vegetal", 9.00, "Pan de chapata", true, 4, true);
		gestor.altaBocadillos(11.50, "Tortilla de patata", "Trastorno heavy de tortilla de patata", 9.00, "Baguete", true, 7, false);
		gestor.altaBocadillos(6.00, "Nocilla","Lunatico brutalico de sandwich de nocilla", 6.00, "Pan tostado bimbo", false, 1, false);
		gestor.altaBocadillos(12.00, "Bromita","Irracionalidad absurda de PedroSanchez", 10.50, "Pan de hamburgesa", false, 8, true);
		
		
		gestor.altaPastas(15.00, "Carbonara", "Espectacularidad deliciosa de espaguetis a la carbonara", 8.0, "Nata", "Espaguetis", 8);
		gestor.altaPastas(12.00, "Tomate", "Locura de la yaya de macarrones con tomate", 6.0, "Salsa de la yaya", "Macarrones", 10);
		gestor.altaPastas(10.50, "Champiñones", "Onda de divinidad de champiñones", 7.0, "Salsa de champiñones con condimentos", "Espaguetis", 9);
		gestor.altaPastas(9.80, "Roquefort y frutos del bosque", "Santa madona de frutos del bosque con roquefort", 8.0, "Salsa de roquefort y frutos del bosque", "Tortelini", 6);
		gestor.altaPastas(13.00, "Queso", "La divina papaya de tomate y queso", 8.0, "Tomate con queso rayado", "Raviolis", 8);
		gestor.altaPastas(12.00, "Boloñesa", "Onda vital de boloñesa", 5.0, "Boloñesa con nueces", "Espaguetis", 10);
		gestor.altaPastas(15.00, "Bromita", "Receta intensiva del chef Zapatero", 8.0, "", "Espaguetis", 8);

		
		gestor.altaBollerias(2.00, "Chocolate", "Merienda explosional de napolitana de chocolate", 3.00, "Napolitana", true, true);
		gestor.altaBollerias(4.00, "Pizza", "Excepcional manjar de bocapizza de jamon york y queso", 5.00, "Bocapizza", false, false);
		gestor.altaBollerias(3.00, "Crema", "Explosion de sabor de crema", 3.00, "Cruasant", true, false);
		gestor.altaBollerias(2.00, "Jamon york y queso", "Recreos de fantasia de napolitana de jamon york", 3.00, "Napolitana", false, false);
		gestor.altaBollerias(3.00, "Chocolate", "Infancia resumida de flauta de chocolate", 3.00, "Flauta de chocolate", true, true);
		gestor.altaBollerias(2.00, "Dulce", "Redondo de la felicidad maravillosa de donut", 4.00, "Donut", true, false);
		gestor.altaBollerias(5.00, "Dulce", "Delicia al paladar de torrijas importadas de la cocina de la yaya", 4.00, "Torrijas", true, false);
		
	
		//╚╔╩╦╠═╬ ╣
		boolean error = false;
		do {
			try {
			System.out.println("╠═══════════════════════════════════════════════════════╣");
			System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
			System.out.println("╠═══════════════════════════════════════════════════════╣");
			System.out.println("╠ Bienvenido al bar de La Locura                        ╣");
			System.out.println("╠═══════════════════════════════════════════════════════╣");
			System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
			System.out.println("╠ Digame que desea saber seleccionando una opcion:      ╣");
			System.out.println("╠ 1- Opciones de pastel                                 ╣");
			System.out.println("╠ 2- Opciones de bocadillo                              ╣");
			System.out.println("╠ 3- Opciones de pasta                                  ╣");
			System.out.println("╠ 4- Opciones de bolleria                               ╣");
			System.out.println("╠ 5- Lista de productos                                 ╣");
			System.out.println("╠ 6- Salir de la bar                                    ╣");
			System.out.println("╠═══════════════════════════════════════════════════════╣");
			System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
			opcion = input.nextInt();
			input.nextLine();
			if (opcion < 1 || opcion >67) {
				System.out.println("Error el valor debe estar entre 1 y 6");
				error = true;
			} else {
				error = false;
			}
			} catch (NumberFormatException e) {
				System.out.println("Error, debes introducir un numero");
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Error de acceso a la informacion del teclado");
				System.exit(0);
			}
			switch (opcion) {
			
			case 1:
				int opcionPastel;
				do {
				System.out.println("Estas dentro del menu de pastel");
				System.out.println("Seleccione una opcion");
				System.out.println("╠═══════════════════════════════════════════════════════╣");
				System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
				System.out.println("╠ Seleccione una opcion:                                ╣");
				System.out.println("╠ 1- Dar de alta un pastel                              ╣");
				System.out.println("╠ 2- Listar pasteles                                    ╣");
				System.out.println("╠ 3- Buscar un pastel                                   ╣");
				System.out.println("╠ 4- Eliminar un pastel                                 ╣");
				System.out.println("╠ 5- Volver al menu principal                           ╣");
				System.out.println("╠═══════════════════════════════════════════════════════╣");
				System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
				opcionPastel = input.nextInt();
				
					switch (opcionPastel) {
					case 1: 
						System.out.println("Introduce el precio del pastel");
						double precioPastel=input.nextDouble();
						input.nextLine();
						
						System.out.println("Introduce el sabor del pastel");
						String saborPastel=input.nextLine();
						
						System.out.println("Introduce el nombre del pastel");
						String nombrePastel=input.nextLine();
						
						System.out.println("Introduce el tamaño del pastel");
						double tamanoPastel=input.nextDouble();
						input.nextLine();
						
						System.out.println("Introduce el tipo de pastel");
						String tipoPastel=input.nextLine();
						
						System.out.println("Introduce el peso del pastel");
						double pesoPastel=input.nextDouble();
						
						System.out.println("Introduce la figura que tendra el pastel");
						String figuraPastel=input.nextLine();
						input.nextLine();
						
						System.out.println("Procedemos a crear su pastel: "+ precioPastel +" "+saborPastel +" "+nombrePastel+" " +tamanoPastel+" " +tipoPastel +" " +pesoPastel +" " +figuraPastel);
						gestor.altaPasteles(precioPastel, saborPastel, nombrePastel, tamanoPastel, tipoPastel, pesoPastel, figuraPastel);
						System.out.println("Su pastel se ha creado correctamente, gracias por confiar en nosotros ^^");
						break;
						
					case 2:
						System.out.println("Mostrando la lista de pasteles...");
						gestor.listarPasteles();
						break;
						
						
					case 3:
						
						input.nextLine();
						System.out.println("Dame un nombre para poder buscar el tipo de su pastel");
						String buscarNombrePastel=input.nextLine();
						
						gestor.buscarPasteles(buscarNombrePastel);
						System.out.println("Su pastel es: "+ gestor.buscarPasteles(buscarNombrePastel).getTipoPastel());
						break;
						
					case 4:
						input.nextLine();
						System.out.println("Dame un nombre para poder eliminar su pastel");
						String eliminarNombrePastel=input.nextLine();
						
						gestor.eliminarPastel(eliminarNombrePastel);
						System.out.println("Su pastel ha sido eliminado con exito");
						break;
						
					case 5:
						System.out.println("Volviendo al menu principal...");
						break;
						
						
					default:
						System.out.println("Opcion erronea");
						System.out.println("Opcion no valida");
						break;
						
					}
					
					
				}while (opcionPastel !=5);
				break;

			case 2:
				int opcionBocadillo;
				do {
				System.out.println("Estas dentro del menu de bocadillo");
				System.out.println("Seleccione una opcion");
				System.out.println("╠═══════════════════════════════════════════════════════╣");
				System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
				System.out.println("╠ Seleccione una opcion:                                ╣");
				System.out.println("╠ 1- Dar de alta un bocadillo                           ╣");
				System.out.println("╠ 2- Listar bocadillos                                  ╣");
				System.out.println("╠ 3- Buscar un bocadillo                                ╣");
				System.out.println("╠ 4- Eliminar un bocadillo                              ╣");
				System.out.println("╠ 5- Metodo pan                                         ╣");
				System.out.println("╠ 6- Volver al menu principal                           ╣");
				System.out.println("╠═══════════════════════════════════════════════════════╣");
				System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
				opcionBocadillo = input.nextInt();
				
					switch (opcionBocadillo) {
					case 1: 
						System.out.println("Introduce el precio del bocadillo");
						double preciobocadillo=input.nextDouble();
						
						System.out.println("Introduce el sabor del bocadillo");
						String saborbocadillo=input.nextLine();
						input.nextLine();
						
						System.out.println("Introduce el nombre del bocadillo");
						String nombrebocadillo=input.nextLine();
						
						System.out.println("Introduce el tamaño del bocadillo");
						double tamanobocadillo=input.nextDouble();
						input.nextLine();
						
						System.out.println("Introduce el tipo de bocadillo");
						String tipoPan=input.nextLine();
						
						System.out.println("Introduce si tu bocadillo es salado");
						boolean esSalado=input.nextBoolean();
						
						System.out.println("Introduce el numero de cortes que tendra el bocadillo");
						int numCortes=input.nextInt();
						
						System.out.println("Introduce si tu bocadillo apto para celiacos o no");
						boolean paraCeliacos=input.nextBoolean();
						
						System.out.println("Procedemos a crear su bocadillo: "+ preciobocadillo+" "+saborbocadillo+" "+nombrebocadillo+" "+tamanobocadillo+" "+tipoPan+" "+esSalado+" "+numCortes+" "+paraCeliacos);
						gestor.altaBocadillos(preciobocadillo, saborbocadillo, nombrebocadillo, tamanobocadillo, tipoPan, esSalado, numCortes, paraCeliacos);
						System.out.println("Su bocadillo se ha creado correctamente, gracias por confiar en nosotros ^^");
						break;
						
					case 2:
						System.out.println("Mostrando la lista de bocadillos...");
						gestor.listarBocadillos();
						break;
						
					case 3:
						input.nextLine();
						System.out.println("Dame un nombre para poder buscar el tipo de pan del bocadillo");
						String buscarNombrebocadillo=input.nextLine();
						
						gestor.buscarBocadillo(buscarNombrebocadillo);
						System.out.println("Su bocadillo es: "+ gestor.buscarBocadillo(buscarNombrebocadillo).getTipoPan());
						
						break;
						
					case 4:
						input.nextLine();
						System.out.println("Dame un nombre para poder eliminar su bocadillo");
						String eliminarNombrebocadillo=input.nextLine();
						
						gestor.eliminarBocadillo(eliminarNombrebocadillo);
						System.out.println("Su bocadillo ha sido eliminado con exito");
						break;
						
					case 5:
						input.nextLine();
						System.out.println("Dame un nombre");
						String buscarNombrebocadilloPan=input.nextLine();
						
						gestor.buscarBocadillo(buscarNombrebocadilloPan).tipoPanNieve();
						
						break;
						
					case 6:
						System.out.println("Volviendo al menu principal...");
						break;
					default:
						System.out.println("Opcion erronea");
						System.out.println("Opcion no valida");
						break;
						
					}
					
					
				}while (opcionBocadillo !=6);
				break;
			case 3:
				int opcionPasta;
				do {
				System.out.println("Estas dentro del menu de Pasta");
				System.out.println("Seleccione una opcion");
				System.out.println("╠═══════════════════════════════════════════════════════╣");
				System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
				System.out.println("╠ Seleccione una opcion:                                ╣");
				System.out.println("╠ 1- Dar de alta un pasta                               ╣");
				System.out.println("╠ 2- Listar Pastas                                      ╣");
				System.out.println("╠ 3- Buscar un Pasta                                    ╣");
				System.out.println("╠ 4- Eliminar un Pasta                                  ╣");
				System.out.println("╠ 5- Volver al menu principal                           ╣");
				System.out.println("╠═══════════════════════════════════════════════════════╣");
				System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
				opcionPasta = input.nextInt();
				input.nextLine();
				
					switch (opcionPasta) {
					case 1: 
						System.out.println("Introduce el precio del Pasta");
						double precioPasta=input.nextDouble();
						input.nextLine();
						
						System.out.println("Introduce el sabor del Pasta");
						String saborPasta=input.nextLine();
						
						System.out.println("Introduce el nombre del Pasta");
						String nombrePasta=input.nextLine();
						
						System.out.println("Introduce el tamaño del Pasta");
						double tamanoPasta=input.nextDouble();
						input.nextLine();
						
						System.out.println("Introduce el tipo de salsa para la pasta");
						String salsa=input.nextLine();
						
						System.out.println("Introduce el tipo de pasta");
						String tipoPasta=input.nextLine();
						
						System.out.println("Introduce el tiempo de coccion para la pasta");
						int tiempoCoccion=input.nextInt();
						
						System.out.println("Introduce si tu Pasta apto para celiacos o no");
						boolean paraCeliacos=input.nextBoolean();
						
						System.out.println("Procedemos a crear su Pasta: "+ precioPasta+" "+saborPasta+" "+nombrePasta+" "+tamanoPasta+" "+salsa+" "+tipoPasta+" "+tiempoCoccion);
						gestor.altaPastas(precioPasta, saborPasta, nombrePasta, tamanoPasta, salsa, tipoPasta, tiempoCoccion);
						System.out.println("Su Pasta se ha creado correctamente, gracias por confiar en nosotros ^^");
						break;
						
					case 2:
						System.out.println("Mostrando la lista de Pastas...");
						gestor.listarPastas();
						break;
						
					case 3:
						System.out.println("Dame un nombre para poder buscar su tipo de pasta");
						String buscarNombrePasta=input.nextLine();
						
						gestor.buscarPasta(buscarNombrePasta);
						System.out.println("Su Pasta es: "+ gestor.buscarPasta(buscarNombrePasta).getTipoPasta());
						
						break;
						
					case 4:
						System.out.println("Dame un nombre para poder eliminar su Pasta");
						String eliminarNombrePasta=input.nextLine();
						
						gestor.eliminarPasta(eliminarNombrePasta);
						System.out.println("Su Pasta ha sido eliminado con exito");
						break;
						
					case 5:
						System.out.println("Volviendo al menu principal...");
						break;
						
					
						
					default:
						System.out.println("Opcion erronea");
						System.out.println("Opcion no valida");
						break;
						
					}
					
					
				}while (opcionPasta !=5);
				
				break;
			case 4:
				int opcionBolleria;
				do {
				System.out.println("Estas dentro del menu de Bolleria");
				System.out.println("Seleccione una opcion");
				System.out.println("╠═══════════════════════════════════════════════════════╣");
				System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
				System.out.println("╠ Seleccione una opcion:                                ╣");
				System.out.println("╠ 1- Dar de alta un bollo                               ╣");
				System.out.println("╠ 2- Listar bollos                                      ╣");
				System.out.println("╠ 3- Buscar un bollo                                    ╣");
				System.out.println("╠ 4- Eliminar un bollo                                  ╣");
				System.out.println("╠ 5- Volver al menu principal                           ╣");
				System.out.println("╠═══════════════════════════════════════════════════════╣");
				System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
				opcionBolleria = input.nextInt();
				
					switch (opcionBolleria) {
					case 1: 
						System.out.println("Introduce el precio del Bolleria");
						double precioBolleria=input.nextDouble();
						input.nextLine();
						
						System.out.println("Introduce el sabor del Bolleria");
						String saborBolleria=input.nextLine();
						
						System.out.println("Introduce el nombre del Bolleria");
						String nombreBolleria=input.nextLine();
						
						System.out.println("Introduce el tamaño del Bolleria");
						double tamanoBolleria=input.nextDouble();
						input.nextLine();
						
						System.out.println("Introduce el tipo de Bolleria");
						String tipoBolleria=input.nextLine();
						
						System.out.println("Introduce si tu bollo es dulce o no");
						boolean esDulce=input.nextBoolean();
						
						System.out.println("Introduce si tu bollo contiene virutas o no");
						boolean conVirutas=input.nextBoolean();
						
						System.out.println("Procedemos a crear su Bolleria: "+ precioBolleria+" "+saborBolleria+" "+nombreBolleria+" "+tamanoBolleria+" "+tipoBolleria+" "+ esDulce+" "+conVirutas);
						gestor.altaBollerias(precioBolleria, saborBolleria, nombreBolleria, tamanoBolleria, tipoBolleria, esDulce, conVirutas);
						System.out.println("Su Bolleria se ha creado correctamente, gracias por confiar en nosotros ^^");
						break;
						
					case 2:
						System.out.println("Mostrando la lista de Bollerias...");
						gestor.listarBollerias();
						break;
					case 3:
						input.nextLine();
						System.out.println("Dame un nombre para poder buscar su tipo de bolleria");
						String buscarNombreBolleria=input.nextLine();
						
						gestor.buscarBolleria(buscarNombreBolleria);
						System.out.println("Su Bolleria es: "+ gestor.buscarBolleria(buscarNombreBolleria).getTipoBolleria());
						break;
						
					case 4:
						input.nextLine();
						System.out.println("Dame un nombre para poder eliminar su Bolleria");
						String eliminarNombreBolleria=input.nextLine();
						
						gestor.eliminarBolleria(eliminarNombreBolleria);
						System.out.println("Su Bolleria ha sido eliminado con exito");
						break;
						
					case 5:
						System.out.println("Volviendo al menu principal...");
						break;
						
					
					default:
						System.out.println("Opcion erronea");
						System.out.println("Opcion no valida");
						break;
						
					}
					
					
					
				}while (opcionBolleria !=5);
				break;
				
			case 5:
				int opcionProductos=0;
				do {
					System.out.println("Estas dentro del menu de Producto");
					System.out.println("Seleccione una opcion");
					System.out.println("╠═══════════════════════════════════════════════════════╣");
					System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
					System.out.println("╠ Seleccione una opcion:                                ╣");
					System.out.println("╠ 1- Listar productos                                   ╣");
					System.out.println("╠ 2- Volver al menu principal                           ╣");
					System.out.println("╠═══════════════════════════════════════════════════════╣");
					System.out.println("╠ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╬ ╚ ╔ ╩ ╦ ╠ ═ ╣");
					opcionProductos = input.nextInt();
					
					switch (opcionProductos) {
					case 1:
						System.out.println("Mostrando los productos...");
						System.out.println("Mostrando pasteles");
						gestor.listarPasteles();
						
						System.out.println("Mostrando bocadillos");
						gestor.listarBocadillos();
						
						System.out.println("Mostrando pastas");
						gestor.listarPastas();
						
						System.out.println("Mostrando bolleria");
						gestor.listarBollerias();
						break;
						
					case 2:
						System.out.println("Volviendo al menu principal...");
						break;
					default:
						System.out.println("Opcion erronea");
						System.out.println("Opcion no valida");
						break;
						
					}
				}while (opcionProductos !=2);
				
				
				break;
			case 6:
				System.out.println("Saliendo del bar");
				break;
			default:
				System.out.println("Opcion no valida, vuelva a elegir opcion");
				break;

			}

		} while (opcion != 6);

		input.close();
	}
}